#!/usr/bin/env perl
# Copyright (c) 2009, 2010
#                      Robert Hülle, <robert.hulle@gmail.com>
#                      Martin Kopta, <martin@kopta.eu>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

use 5.008;
use strict;
use warnings;

my @test;

$test[0] = {
  special     => 1,
  name        => 'filetype',
  depends     => [],
  description => 'filetype test',
  types       => [],
  params      => [qw/file full_name types/],
  test        => sub {
    my ($file, $full_file, $types) = @_;
    @$types = ();
    if ( -f $full_file ) {
      push @$types, 'file';
    }
    if ( -d $full_file ) {
      push @$types, 'dir';
    } elsif ( $file =~ m/\.mp3$/i ) {
      push @$types, 'mp3';
    } elsif ( $file =~ m/\.ogg$/i ) {
      push @$types, 'ogg';
    }
    return scalar @$types;
  },
};

push @test, {
  name        => 'directory_not_empty',
  depends     => [],
  description => 'mp3 suffix test',
  types       => [qw/dir/],
  params      => [qw/full_name/],
  test        => sub {
    my ($directory_name) = @_;
    opendir (my $dir, $directory_name) or do {
      warn "Can not open directory \'$directory_name\': $!\n";
      return undef;
    };
    my $res = grep !m/^\.\.?$/, readdir($dir);
    closedir $dir;
    return $res;
  },
};

push @test, {
  name        => 'mp3_suffix',
  depends     => [],
  description => 'mp3 suffix test',
  types       => [qw/mp3/],
  params      => [qw/file/],
  test        => sub {
    my ($file) = @_;
    return $file =~ m/\.mp3$/;
  },
};

push @test, {
  name        => 'ogg_suffix',
  depends     => [],
  description => 'ogg suffix test',
  types       => [qw/ogg/],
  params      => [qw/file/],
  test        => sub {
    my ($file) = @_;
    return $file =~ m/\.ogg$/;
  },
};

push @test, {
  name        => 'file_prefix',
  depends     => [],
  description => 'file prefix test',
  types       => [qw/mp3 ogg/],
  params      => [qw/file/],
  test        => sub {
    my ($file) = @_;
    return $file =~ m/^\d{1,3}\ -\ /;
  },
};

push @test, {
  name        => 'path_depth',
  depends     => [],
  description => 'path depth test',
  types       => [qw/mp3 ogg/],
  params      => [qw/dirs/],
  test        => sub {
    my ($dirs) = @_;
    return @$dirs >= 2 && @$dirs <= 3;
  },
};

push @test, {
  name        => 'path_name',
  depends     => ['path_depth'],
  description => 'path name test',
  types       => [qw/mp3 ogg/],
  params      => [qw/dirs/],
  test        => sub {
    my ($dirs) = @_;
    return @$dirs < 3 || $dirs->[2] =~ m/^cd\d$/;
  },
};

push @test, {
  name        => 'executablity',
  depends     => [],
  description => 'path depth test',
  types       => [qw/mp3 ogg/],
  params      => [qw/full_name/],
  test        => sub {
    my ($file) = @_;
    return !(-x $file);
  },
};

push @test, {
  name        => 'mp3_bitrate',
  depends     => [],
  description => 'mp3 bitrate test',
  types       => [qw/mp3/],
  params      => [qw/full_name/],
  test        => sub {
    my ($file) = @_;
    use MP3::Info;
    my $mp3 = get_mp3info($file);
    unless (defined $mp3) {
      return 0;
    }
    if ($mp3->{VBR} || (($mp3->{BITRATE} // 0) >= 128) ) {
      return 1;
    } else {
      return 0;
    }
  },
};

push @test, {
  name        => 'mp3_tag',
  depends     => [],
  description => 'id3 tag presence test',
  types       => [qw/mp3/],
  params      => [qw/full_name/],
  test        => sub {
    my ($file) = @_;
    use MP3::Tag 1.11;
    my $mp3 = MP3::Tag->new($file);
    $mp3->config(prohibit_v24 => 0);
    $mp3->get_tags;
    if (exists($mp3->{ID3v1}) || exists($mp3->{ID3v2})) {
      return 1;
    } else {
      return 0;
    }
  },
};

push @test, {
  name        => 'path_one_space',
  depends     => [],
  description => 'only one space in path test',
  types       => [qw/mp3 ogg/],
  params      => [qw/dirs/],
  test        => sub {
    my ($dirs) = @_;
    return (grep !m/\ \ /, @$dirs) == @$dirs
  },
};

push @test, {
  name        => 'file_one_space',
  depends     => [],
  description => 'only one space in filename test',
  types       => [qw/mp3 ogg/],
  params      => [qw/file/],
  test        => sub {
    my ($file) = @_;
    return $file !~ m/\ \ /;
  },
};

push @test, {
  name        => 'path_only_space',
  depends     => [],
  description => 'space only whitespace in path test',
  types       => [qw/mp3 ogg/],
  params      => [qw/dirs/],
  test        => sub {
    my ($dirs) = @_;
    for my $dir (@$dirs) {
      return 0 if ( () = $dir =~ m/(\ )/g ) != ( () = $dir =~ m/(\s)/g );
    }
    return 1;
  },
};

push @test, {
  name        => 'file_only_space',
  depends     => [],
  description => 'space only whitespace in filename test',
  types       => [qw/mp3 ogg/],
  params      => [qw/file/],
  test        => sub {
    my ($file) = @_;
    return ( () = $file =~ m/(\ )/g ) == ( () = $file =~ m/(\s)/g );
  },
};

push @test, {
  name        => 'file_no_symlink',
  depends     => [],
  description => 'no file is symlink test',
  types       => [qw/mp3 ogg/],
  params      => [qw/full_name/],
  test        => sub {
    my ($file) = @_;
    return ! -l $file;
  },
};

push @test, {
  name        => 'mp3_tag_artist',
  depends     => ['mp3_tag'],
  description => 'id3 artist tag presence test',
  types       => [qw/mp3/],
  params      => [qw/full_name/],
  test        => sub {
    my ($file) = @_;
    use MP3::Tag 1.11;
    my $mp3 = MP3::Tag->new($file);
    $mp3->config(prohibit_v24 => 0);
    $mp3->config(autoinfo => 'ID3v1', 'ID3v2');
    $mp3->get_tags;
    return $mp3->performer;
  },
};

push @test, {
  name        => 'mp3_tag_album',
  depends     => ['mp3_tag'],
  description => 'id3 album tag presence test',
  types       => [qw/mp3/],
  params      => [qw/full_name/],
  test        => sub {
    my ($file) = @_;
    use MP3::Tag 1.11;
    my $mp3 = MP3::Tag->new($file);
    $mp3->config(prohibit_v24 => 0);
    $mp3->config(autoinfo => 'ID3v1', 'ID3v2');
    $mp3->get_tags;
    return $mp3->album;
  },
};

push @test, {
  name        => 'mp3_tag_title',
  depends     => ['mp3_tag'],
  description => 'id3 title tag presence test',
  types       => [qw/mp3/],
  params      => [qw/full_name/],
  test        => sub {
    my ($file) = @_;
    use MP3::Tag 1.11;
    my $mp3 = MP3::Tag->new($file);
    $mp3->config(prohibit_v24 => 0);
    $mp3->config(autoinfo => 'ID3v1', 'ID3v2');
    $mp3->get_tags;
    return $mp3->title;
  },
};

push @test, {
  name        => 'mp3_tag_track',
  depends     => ['mp3_tag'],
  description => 'id3 track tag presence test',
  types       => [qw/mp3/],
  params      => [qw/full_name/],
  test        => sub {
    my ($file) = @_;
    use MP3::Tag 1.11;
    my $mp3 = MP3::Tag->new($file);
    $mp3->config(prohibit_v24 => 0);
    $mp3->config(autoinfo => 'ID3v1', 'ID3v2');
    $mp3->get_tags;
    return $mp3->track;
  },
};


# gets argument from command line
# first argument is "music library root directory" (path)
# any other arguments are ignored
my $mlroot = shift;
my @file;

unless (defined $mlroot) {
  print "Usage: ".$0." <mlroot>\n";
  exit 0;
}

# Change dir to music library
# needed by some tests and reducing impact of error
chdir $mlroot || die "Can not chdir to \'$mlroot\': $!\n";

# Go thru all music library and test all files
find_files(\@file, $mlroot);
my @error = run_tests(\@file);

# Prints result
print "\n";
if (@error > 0) {
  print "Errors: ".@error."\n";
} else {
  print "Your music library is MLHS compatible.\n";
}

#################################################################

# For given path returns all files in (recursive, sorted)
sub find_files {
  my ($files, $root_directory) = @_;
  use File::Find;
  find({
    wanted => sub {
      use File::Spec;
      if ( -e $_ ) {
        my ($volume, $path, $file) = File::Spec->splitpath($_);
        $path = File::Spec->abs2rel($path, $root_directory);
        my @dirs = grep $_ ne '', File::Spec->splitdir($path);
        push @$files, {
            full_name => File::Spec->abs2rel($_,$root_directory),
            path      => $path,
            file      => $file,
            dirs      => \@dirs,
          };
      }
    },
    no_chdir => 1,
  }, $root_directory);
  @$files = sort {
    $a->{full_name} cmp $b->{full_name}
  } @$files;
}

sub run_tests {
  my ($files) = @_;
  my @error;
  for my $file (@$files) {
    $file->{types} = [];
TEST:
    for my $test (@test) {
      if (@{$test->{types}}) {
        my $is_type = 0;
        for my $type (@{$test->{types}}) {
          $is_type += () = grep $_ eq $type, @{$file->{types}};
        }
        next unless $is_type;
      }
      for my $dependency ( @{$test->{depends}} ) {
        unless ($file->{test_ok}{$dependency}) {
          print_error($file, $test, 1);
          next TEST;
        }
      }
      my @param = ();
      for my $requested_param (@{$test->{params}}) {
        if ( $requested_param eq 'file' ) {
          push @param, $file->{file};
        } elsif ( $requested_param eq 'dirs' ) {
          push @param, $file->{dirs};
        } elsif ( $requested_param eq 'full_name' ) {
          push @param, $file->{full_name};
        } elsif ( $requested_param =~ m/^(\\)?types$/ ) {
          push @param, $1?\$file->{types}:$file->{types};
        } else {
          warn 'Unknown parameter: '.$requested_param.' at '.__LINE__;
        }
      }
      my $result = $test->{test}(@param);
      unless ($result) {
        print_error($file, $test);
        push @error, { test => $test, file => $file };
      } else {
        $file->{test_ok}{$test->{name}} = 1;
      }
    }
  }
  return @error;
}

sub print_error {
  my ($file, $test, $skipped) = @_;
  unless ($file->{print_error}) {
    print "File \'", $file->{full_name}, "\':\n";
  }
  $file->{print_error} += 1;
  if ($skipped) {
    print '  skipped: ', $test->{name}, "\n"
  } else {
    print '  failed:  ', $test->{name}, "\n"
  }
}

__END__
=head1 NAME

mlhs-validator - check your music library to MLHS compatibility

=head1 SYNOPSIS

mlhs-validator <directory>

=head1 DESCRIPTION

Validates given music library according to MLHS standard.

=head1 LICENSE

MIT

=cut
