#!/usr/bin/env bash
# Copyright (c) 2010, Martin Kopta <martin@kopta.eu>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

declare -r VERSION="0.0"
declare -i number_of_errors=0
declare -i minimum_sbr_bitrate=192
declare -i log=1
declare logfile="/tmp/mlhs-validator-output"
declare collection=""

function error {
  [ ${log} -ne 0 ] && echo "$@" >> "${logfile}"
  echo "$@"
  let number_of_errors++
}

function is_dir {
  local dir=$1
  if [ -d "${dir}" ]; then
    true
  else
    error "'${dir}' is not directory"
    false
  fi
}

function is_dir_empty {
  local dir=$1
  local dir_content=$(ls "${dir}")
  if [ -z "${dir_content}" ]; then
    error "'${dir}' is empty directory"
    true
  else
    false
  fi
}

function is_file {
  local file=$1
  if [ -f "${file}" ]; then
    true
  else
    error "'${file}' is not a file"
    false
  fi
}

function has_double_space {
  if [[ "${1}" =~ \ \  ]]; then
    error "'${1}' contains two or more spaces next to each other"
    return 1
  fi
  return 0
}

function check_collection {
  local collection=$1
  local -i retval=0
  ! is_dir "${collection}" && return 1
  is_dir_empty "${collection}" && return 1
  for artist in "${collection}"/*; do
    check_artist "${artist}"
    let retval\|=$?
  done
  return ${retval}
}

function check_artist {
  local artist=$1
  local -i retval=0
  local artist_basename=""
  ! is_dir "${artist}" && return 1
  is_dir_empty "${artist}" && return 1
  artist_basename=$(basename "${artist}")
  if [[ "${artist_basename}" =~ \ \  ]]; then
    error "'${artist}' contains two or more spaces next to each other"
    retval=1
  fi
  for album in "${artist}"/*; do
    check_album "${album}"
    let retval\|=$?
  done
  return ${retval}
}

function check_album {
  local album=$1
  local -i retval=0
  local album_basename=""
  local firstitem=""
  ! is_dir "${album}" && return 1
  is_dir_empty "${album}" && return 1
  album_basename=$(basename "${album}")
  if [[ "${album_basename}" =~ \ \  ]]; then
    error "'${album}' contains two or more spaces next to each other"
    retval=1
  fi
  firstitem=$(ls "${album}" | head -n1)
  if [ -d "${firstitem}" ]; then
    check_volumes "${album}"
    let retval\|=$?
  else
    check_songs "${album}"
    let retval\|=$?
  fi
  return ${retval}
}

function check_volumes {
  local album=$1
  local -i retval=0
  local -i volume_no=1
  for volume in "${album}"/*; do
    check_volume "${volume}" ${volume_no}
    let retval\|=$?
    let volume_no++
  done
  return ${retval}
}

function check_volume {
  local volume=$1
  local -i retval=0
  local -i volume_number=$2
  if ! [[ "${volume}" =~ cd[0-9] ]]; then
    error "'${volume}' volume does not follow cd[0-9] pattern"
    return 1
  fi
  if ! [[ "${volume}" =~ cd0*${volume_number} ]]; then
    error "'${volume}' volume has unexpected rank. Sequence corrupted."
    retval=1
  fi
  is_dir_empty "${volume}" && return 1
  check_songs "${volume}"
  let retval\|=$?
  return ${retval}
}

function check_songs {
  local songdir=$1
  local -i retval=0
  local -i song_no=1
  for song in "${songdir}"/*; do
    check_song "${song}" ${song_no}
    let retval\|=$?
    let song_no++
  done
  return ${retval}
}

function check_song {
  local song=$1
  local -i retval=0
  local -i song_no=$2
  local song_basename=""
  local song_suffix=""
  ! is_file "${song}" && return 1
  if [ -L "${song}" ]; then
    error "'${song}' is a symlink"
    return 1
  fi
  song_basename=$(basename "${song}")
  if [[ "${song_basename}" =~ \ \  ]]; then
    error "'${song}' contains two or more spaces next to each other"
    retval=1
  fi
  song_suffix=$(echo "${song_basename}" | sed -r 's/.*\.([^.]+)$/\1/')
  if [ "${song_suffix}" != "mp3" ] \
    && [ "${song_suffix}" != "flac" ] \
    && [ "${song_suffix}" != "ogg" ]
  then
     error "'${song}' does not have allowed suffix (mp3, flac or ogg)"
     return 1
  fi
  if ! [[ "${song_basename}" =~ ^[0-9][0-9]\ -\  ]]; then
    error \
     "'${song}' doesn't begin with numbers followed by space surrounded dash"
    retval=1
  fi
  if ! [[ "${song_basename}" =~ ^0*${song_no}\ -\  ]]; then
    error "'${song}' has unexpected rank. Sequence corrupted."
    retval=1
  fi
  if [ -x "${song}" ]; then
    error "'${song}' has executable flag set"
    retval=1
  fi
  case "${song_suffix}" in
    "mp3")
      check_mp3 "${song}"
      let retval\|=$?
      ;;
    "flac")
      check_flac "${song}"
      let retval\|=$?
      ;;
    "ogg")
      check_ogg "${song}"
      let retval\|=$?
      ;;
    *)
      echo "Internal error in function 'check_song'" 1>&2
      exit 1
  esac
  return ${retval}
}

function get_from_mediainfo_output {
  local mediainfo_output=$1
  local keyword=$2
  local value=$(
    echo "${mediainfo_output}" \
    | grep "^${keyword}  " \
    | head -n 1 \
    | sed -r 's/^[^:]+: //'
  )
  echo "${value}"
}

function check_mp3 {
  local file=$1
  local mediainfo_output=$(mediainfo "${file}")
  local -i retval=0
  local file_format=""
  local file_format_profile=""
  local file_bitrate_mode=""
  local -i file_bitrate=0
  if [ -z "${mediainfo_output}" ]; then
    echo -n "Internal error. " 1>&2
    echo "Cannot get any information from mediainfo about '${file}'" 1>&2
    exit 1
  fi
  file_format=$(get_from_mediainfo_output "${mediainfo_output}" "Format")
  if [ "${file_format}" != "MPEG Audio" ]; then
    error "'${file}' is not MPEG Audio"
    return 1
  fi
  file_format_profile=$(
    get_from_mediainfo_output "${mediainfo_output}" "Format profile")
  if [ "${file_format_profile}" != "Layer 3" ]; then
    error "'${file}' is not MPEG Audio Layer 3"
    return 1
  fi
  file_bitrate_mode=$(
    get_from_mediainfo_output "${mediainfo_output}" "Bit rate mode")
  if [ "${file_bitrate_mode}" != "Variable" ]; then
    # expecting SBR here
    file_bitrate=$(
      get_from_mediainfo_output "${mediainfo_output}" "Bit rate" \
      | sed -r 's/(\.[0-9]+)? Kbps//')
    if [[ "${file_bitrate}" -lt "${minimum_sbr_bitrate}" ]]; then
      error "'${file}' has too low SBR bitrate"
      retval=1
    fi
  fi
  return ${retval}
}

function check_flac {
  local file=$1
  local -i retval=0
# TODO: not implemented
  return ${retval}
}

function check_ogg {
  local file=$1
  local -i retval=0
# TODO: not implemented
  return ${retval}
}

function dependency_test {
  local mediainfo_bin=$(which mediainfo 2>/dev/null)
  if [ -z "${mediainfo_bin}" ]
  then
    echo "Could not find 'mediainfo' binary."
    echo "  MediaInfo homepage: http://mediainfo.sourceforge.net/" 1>&2
    return 1
  fi
  return 0
}

function license {
echo "Copyright (c) 2010, Martin Kopta <martin@kopta.eu>                      "
echo "                                                                        "
echo "Permission to use, copy, modify, and/or distribute this software for any"
echo "purpose with or without fee is hereby granted, provided that the above  "
echo "copyright notice and this permission notice appear in all copies.       "
echo "                                                                        "
echo "THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES"
echo "WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF        "
echo "MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR "
echo "ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  "
echo "WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN   "
echo "ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF "
echo "OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.          "
}

function usage {
  echo "Usage: $0 [opts] <music directory>"
  echo "  -v         version"
  echo "  -h         show this help"
  echo "  -L         show license"
  echo "  -b <num>   minimum allowed SBR bitrate [kbps]"
  echo "             (default ${minimum_sbr_bitrate} kbps)"
  echo "  -n         don't use logfile"
  echo "  -l <file>  set logfile"
  echo "             (default '${logfile}')"
  echo "Note: validator writes output by default to standart output"
  echo "      (stdout) and copy to logfile."
  echo "Released under ISC license."
  echo "Copyright (c) 2010, Martin Kopta <martin@kopta.eu>"
}

function process_args {
  while getopts vhLb:l:n  opt; do
    case "${opt}" in
      v) echo "MLHS validator ${VERSION}"; exit 0 ;;
      h) usage; exit 0 ;;
      L) license; exit 0 ;;
      n) log=0 ;;
      l) logfile="${OPTARG}" ;;
      b) minimum_sbr_bitrate="${OPTARG}" ;;
      \\?)
        echo "Error: unknown flag" 1>&2
        usage
        exit 1
      ;;
    esac
  done
  shift `expr $OPTIND - 1`
  if [ -z "${1}" ]; then
    echo -e "No collection given.\n" 1>&2
    usage
    exit 1
  elif [ -n "${2}" ]; then
    echo -e "Extra argument given ('${2}').\n" 1>&2
    usage
    exit 1
  else
    collection="${1}"
  fi
}

function prepare_logfile {
  [ ${log} -eq 0 ] && return
  touch "${logfile}" &>/dev/null
  if [ $? -ne 0 ]; then
    echo "Cannot use '${logfile}'. Please, check file permissions." 1>&2
    echo "Logging off." 1>&2
    sleep 5
    log=0
  else
    # clear file
    > "${logfile}"
    if [ $? -ne 0 ]; then
      echo -n "Cannot write into '${logfile}'. "
      echo "Please, check file permissions." 1>&2
      echo "Logging off." 1>&2
      sleep 5
      log=0
    fi
  fi
}

function main {
  process_args "$@"
  dependency_test
  [ $? -ne 0 ] && return 1
  prepare_logfile
  check_collection "${collection}"
  if [ $? -ne 0 ]; then
    echo "------------------------"
    echo "Number of errors: ${number_of_errors}"
  fi
  return 0
}

main "$@"
exit $?
