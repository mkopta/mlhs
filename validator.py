#!/usr/bin/env python3
import os, re

def check_library(path):
    check_artists(path)

def check_artists(path):
    artists = sorted(os.listdir(path))
    if not artists:
        print('Missing artists in: ' + path)
    for artist in artists:
        if not os.path.isdir(artist):
            print('Not a directory: ' + artist)
            continue
        check_albums(path + '/' + artist)

def check_albums(path):
    albums = sorted(os.listdir(path))
    if not albums:
        print('Missing albums in: ' + path)
    for album in albums:
        albumfullpath = path + '/' + album
        if not os.path.isdir(albumfullpath):
            print('Not a directory: ' + albumfullpath)
            continue
        check_songs_or_cds(albumfullpath)

def check_songs_or_cds(path):
    items = sorted(os.listdir(path))
    if not items:
        print('Missing songs in: ' + path)
        return
    itemsfullpath = [path + '/' + i for i in items]
    all_dirs = all(map(lambda i: os.path.isdir(i), itemsfullpath))
    all_files = all(map(lambda i: os.path.isfile(i), itemsfullpath))
    if not (all_dirs or all_files):
        print('Garbage in: ' + path)
    elif all_dirs:
        check_cds(path)
    elif all_files:
        check_songs(path)

def check_cds(path):
    broken_seq_reported = False
    cds = sorted(os.listdir(path))
    if not cds:
        print('Missing cds in: ' + path)
    i = 0
    for cd in cds:
        i += 1
        if not re.search('^cd[1-9]$', cd):
            print('Invalid CD name in: ' + path + ' (' + cd + ')')
            continue
        elif cd != 'cd' + str(i):
            if not broken_seq_reported:
                print('Broken CD number sequence in: ' + path)
                broken_seq_reported = True
            continue
        check_songs(path + '/' + cd)

def check_songs(path):
    broken_seq_reported = False
    songs = sorted(os.listdir(path))
    i = 0
    for song in songs:
        fullsongpath = path + '/' + song
        i += 1
        seq = str(i)
        if i < 10:
            seq = '0' + seq
        if not re.search('^[0-9][0-9] - .*\.(mp3|ogg|flac)$', song):
            print('Invalid song name: ' + fullsongpath)
            continue
        elif not song.startswith(seq):
            if not broken_seq_reported:
                print('Broken song number sequence in: ' + path)
                broken_seq_reported = True
        if re.search('[\t\n\r\f\v]', song):
            print('Unexpected whitespace in song name: ' + fullsongpath)
        if re.search('[ ]{2}', song):
            print('Multiple spaces in song name: ' + fullsongpath)

check_library('.')
